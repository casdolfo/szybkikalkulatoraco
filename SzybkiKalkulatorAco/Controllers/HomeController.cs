﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using SzybkiKalkulatorAco.Models;

namespace SzybkiKalkulatorAco.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        [HttpPost]
        public IActionResult Calculator(CalculatorModel calc) //Kontroler kalkulatora
        {
            switch (calc.calculate)
            {
                case "+":
                    calc.result = calc.number1 + calc.number2;
                    break;
                case "-":
                    calc.result = calc.number1 - calc.number2;
                    break;
                case "*":
                    calc.result = calc.number1 * calc.number2;
                    break;
                case "/":
                        calc.result = calc.number1 / calc.number2;
                    break;
            }
            ViewData["result"] = calc.result; //przekazanie wyniku z kontrolera do widoku.

            return View("Index"); //zwraca widok index.cshtml
        }


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
