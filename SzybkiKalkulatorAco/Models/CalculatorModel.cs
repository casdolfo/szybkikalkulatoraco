﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SzybkiKalkulatorAco.Models
{
    public class CalculatorModel //model kalkulatora
    {
        public int number1 { get; set; }
        public int number2 { get; set; }
        public string calculate { get; set; }
        public int result { get; set; }

    }
}
